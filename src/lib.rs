#![deny(missing_docs)]

//! This crate provides functionality for parsing markdown style headers.

/// Represents the changes to be made to a path.
///
/// This is generated when a header is found.
/// It's still possible to access the previous path of headers before applying the changes.
#[must_use]
pub struct PathChanges<'a> {
    drop: usize,
    /// The header name that will be added.
    pub header: Box<str>,
    /// The path to which changes will be applied.
    pub path: &'a mut Vec<Box<str>>,
}

impl PathChanges<'_> {
    /// Applies the changes to the path.
    pub fn apply(self) {
        let Self { drop, header, path } = self;

        for _ in 0..drop {
            path.pop();
        }

        path.push(header);
    }
}

/// Indicates that a subheader was found without a corresponding header.
pub struct SubheaderWithoutHeader;

/// Parses a header from a line of text.
///
/// # Arguments
///
/// * `path`: A list of headers including the current header.
/// * `line`: The line of text to parse.
///
/// # Returns
///
/// If the line is not a header, returns `None`.
/// If the line is a valid header, returns the `PathChanges` which have to be used to update the path.
/// If the header is a subheader without a corresponding header, an error is returned.
pub fn parse_header<'a>(
    path: &'a mut Vec<Box<str>>,
    line: &str,
) -> Option<Result<PathChanges<'a>, SubheaderWithoutHeader>> {
    let mut start = 0;

    let mut chars = line.chars();
    while Some('#') == chars.next() {
        start += 1;
    }

    if start == 0 {
        return None;
    }

    let level = start - 1;

    let len = path.len();

    Some(if len < level {
        Err(SubheaderWithoutHeader)
    } else {
        Ok(PathChanges {
            drop: len - level,
            header: line[start..].trim().into(),
            path,
        })
    })
}
